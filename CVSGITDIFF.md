title: CVS diffs
author: 
    name: Antanas Sinica/Jurgis Bridzius
output: cvs.html
controls: false

--

##Differences with CVS
###Answers via http://stackoverflow.com/questions/802573/difference-between-git-and-cvs

--
 
* Git, unlike CVS, does not use a centralised version control info storage place like CVSROOT. In git, every repo has a .git folder, where the settings for that repo are stored. This makes it very easy to set up new/existing repositories.

--

* No need to have a central place where one commits his changes. Each developer could have his own repo and fetch/merge changes from other devs repos as he wants. Though it is still a well-known standard to have a unified repo that at least collects all the changes.

--

* Lots of standard, command-line, tooling available for git out of the box - git diff (show differences between branches/commits), git blame (find author of merge/conflict), git bisect (find revision/commit that introduced a bug)

--

* Commits:
    + Git: commits and other operations are atomic - either they happen 100% or don't happen at all
    + CVS: it's possible for an operation to be interrupted and the repo is left inconsitent
    + Git: Since commiting and publishing are two different actions on git, it's easy to amend commits (edit commit that has already been done) before pushing it into the HEAD repo.

--

* Changesets: 
    + Per-project in git, vs per-file in CVS, which is quite a difference of rationale. No GNU Changelog in Git. Also, much easier to revert or undo whole changes, but makes partial reverts impossible.

--

* Merging:
    + Git: commit-before-merge workflow. You have to commit the state of your project into your local repo before merging changes coming from ther devs. Commit-merge-recommit workflow, similar to CVS is possible via git rebase, where your changes are replayed on an updated HEAD.
    + CVS: merge-before-commit workflow. If you want to commit and there are changes, you first have to merge the changes into your branch and then solve conflicts/commit

--

* Tagging:
    + CVS - a tag might represent how many times a file has been updated and is part of a changelog.
    + Git - a tag is added manually, to represent a "state" of the WHOLE project and links to a commit SHA in a branch. Completely optional.

--

* Branching:  
    + CVS: Have to tag branches for them to have a name. No merge tracking, So you have to tag branches again when merged or, even worse, remember when you did it.
    + Git: Branching and merging is easy - git does everything for you, except choose the branch name. It was created for distributive development, of which a huge part is working on separate dev branches.
 
--

* Renaming:
    + CVS: Not supported - might cause a few breakages
    + Git: Uses heuristic rename detection, which compares files via similarity of content and filename - renaming a file would trigger a change in commit and be noted when merging.
