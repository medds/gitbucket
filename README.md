title: What is git?
author: 
    name: Antanas Sinica/Jurgis Bridzius
output: basic.html
controls: false

--

 # Advocating Git
 
 ## From Swedish Banking with ♥

 <br />

WORKSHOP day 1: Versioning-to-deployment domain
_2016-02-17_


--

 #The year was 2005

--

 ###And this guy 

 ![Image of Linus](linus1.png)
 ####Was tired of BitTracker

--

 ####By tired we mean that BitTracker was becoming slow and, worst of all, paid.

--

 ###But he needed a place to put this guy
 <div style="text-align: center">
     <img src="tux.png" style="height: 350px"></img>
 </div>
--

 ###So he created
 <div style="text-align: center">
  ![GIT](git.jpg)
</div>

--

##What is Git?
 * Started by Linux Torvalds, to hold the Linus Kernel in 2005
 * Recently celebrated 10 year old birthday
 * HTTP based version control system with a quite complex CLI
 * Most commonly used in the world ATM (Surpassed SVN with 33% usage overall, according to 2014 Eclipse survey)
 * Shell/Bash wrapper clients already available - TortoiseGit, for example.
 * Many GUI clients already developed - Sourcetree, Git Extensions, SmartGit, GitKraken
 * Native integration with many popular IDEs - whole JetBrains suite, Eclipse, lots of Text-based IDEs. 
 * Many large companies already using/switching to git from Perforce/SVN. At least 1 in 3 fortune 500 companies use a git repo for production code.
 * Noone should use CVS. Ever.

--

#### If DAS REPO goes down - you continue working and commiting locally.

--

###Bitbucket
####The Australian repository solution

--

* Introduced by Atlassian, the creators of JIRA, Confluence etc.
* Closed source, but used all over the world, either as a free cloud hosting solution or as an internal corporate tool
* Easy to set up and maintain
* Easy integration with JIRA (already used in Swedbank), Confluence(Already used in Swedbank), Bamboo and other Atlassian tools.
* Supports fast scaling (provided, you buy the correct license
* Frequent security updates
* Supports Markdown for internal code documentation
* SOME plugin support

--

###Swedbank Bitbucket Server

--

* Magically, git, along with several helpful extensions is already available via toolbox
* Currently hosted on http://stash.sbcore.net:7990 - subject to change soon
* Mainly used for storing javascript code for the new swedish internet bank

#### Soon
* SWP VPN access
* https://git.swedbank.net
* JIRA 7 integration
* Official support for code search is in progress by Atlassian


--

##Swedish banking git experience

--

* 3 major projects have been fully released
* ~18 front-end releases already happened using git
* Front-end git release cycle slowed down by backend release cycle
* Using a slightly modified version of the gitflow workflow
--

 <img style="max-width: 100%;" src="gitflow.jpg"></img>

--

### Fun facts
* Swedish Banking still uses RTC for our backend Java applications.

--

### Fun facts
* Swedish Banking still uses RTC for our backend Java applications.
* We want to move to git with that too.

--

###Why should I use git?

--

###Why should I use git?
####Who has done at least 10 git commits in their life?
--

###Why should I use git?
####Who has done at least 10 git commits in their life?
####Who of you thinks, that we should go with git in Swedbank?
--

#Q&A

--

Branching. Develop potentially breaking changes that don't affect "next". How to do it over multiple components & applications? For example, new feature with changes in ibank, LAC, salesman-components, hansa-common. This problem extends from source control to dependency management.

-- 

Version numbering. We're used to include SVN revision in our versions (next-2715, 2.5-1784). Git commit ids are not intuitively differentiable (is next-2a00688 newer or older than next-9142ebd?) and we don't want to manually assign numbers for each build (of which there are many, each as likely to be deployed, but few actually are).

Use semantic versioning like 2.1.&lt;any random incremental value e.g. jenkins build number>


--

###Differences with  SVN

--

 - Basics
     * SVN: Easier to explain to a non-programmer and less complicated command lines than git. (git clone checks out a repo and git checkout switches branches. WTF?)
     * Git: Smaller repo size. WordPress including tags/branches : 176.9 Mb on git vs 3.13 Gb on svn

--

 - Connectivity
     * SVN: Requires connection with remote repo for all actions
     * Git: Only requires connection to remote when doing actual remote tasks - push/pull. Otherwise, al of the work can be done on local repo and only synced sometimes.

--

 - Commits:
     + SVN: Every commit goes to central repo, no private branches are possible
     + Git: All commits first go to a local (private) branch, and only then are pushed to a remote repo.
     + Git: SVN commit is split into three different tasks in git - add/commit/push, which provide more granularity

--

 - Renaming:
     + SVN: Requires invoking specific commands for renaming files
     + Git: Uses heuristic rename detection, which compares files via similarity of content and filename - renaming a file would trigger a change in commit and be noted when merging
--
